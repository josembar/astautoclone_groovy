package Autoclone.Scripts

import Autoclone.Classes.Consultant

Consultant consultant1 = new Consultant(firstName: 'José',lastName: 'Barrantes',jobTitle: 'Developer',dateOfStart: new Date(),firstJob: true)
println(consultant1)

Consultant consultant2 = consultant1.clone()
consultant2.lastName = 'Vides'
consultant2.jobTitle = 'Technician'
println(consultant2)

