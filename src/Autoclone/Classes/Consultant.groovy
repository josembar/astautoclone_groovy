package Autoclone.Classes

import groovy.transform.AutoClone
import groovy.transform.ToString
@AutoClone
@ToString

class Consultant
{
    String firstName,lastName,jobTitle
    Date dateOfStart
    Boolean firstJob

    String getFullName()
    {
        String fullName = "$firstName $lastName"
        return fullName
    }

    void printName()
    {
        println "Consultant's full name is: $fullName"
    }

    void printDateOfStart()
    {
        println "Date of start of consultant $fullName is $dateOfStart"
    }

    void printIsFirstJob()
    {
        if (firstJob)
        {
            println "This is the first job for consultant $fullName"
        }

        else println "This isn't the first job for consultant $fullName"
    }
}
